package testScenarios;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import helper.Constants;

public abstract class TestBase 
{

	public static WebDriver driver;
	
	@Parameters({"browser"})
	@BeforeSuite
	public static void setDriver(String browser)
	{
	
		if(browser.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver",(System.getProperty("user.dir") + "//src//test//resources//Drivers//chromedriver.exe"));
			driver=new ChromeDriver();
		}
		else
		{
			System.setProperty("webdriver.gecko.driver", (System.getProperty("user.dir") + "//src//test//resources//Drivers//geckodriver.exe"));
			driver=new FirefoxDriver();
		}
	
		driver.get(Constants.URL);
		driver.manage().window().maximize();
		driver.navigate().refresh();
	}
	
	@AfterSuite
	public void tearDown()
	{
		try
		{
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
}