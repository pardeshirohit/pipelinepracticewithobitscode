package testScenarios;

import org.testng.annotations.Test;

import pageObject.CommonModel;

public class CommonPage extends TestBase {

	@Test(priority=1)
	public void checkSearchFunctionality()
	{
		CommonModel commonModel=new CommonModel(driver);
		commonModel.checkSearchFunctionalityIsWorkingProperly();
	}
	@Test(priority=2)
	public void checkSignUpFunctionality()
	{
		CommonModel commonModel=new CommonModel(driver);
		commonModel.checkSignUpFunctionalityIsWorkingProperly();
	}
	@Test(priority=3)
	public void checkCalendarFunctionality() throws InterruptedException
	{
		CommonModel commonModel=new CommonModel(driver);
		commonModel.checkCalendarFunctionalityIsWorkingProperly();
	}
}
