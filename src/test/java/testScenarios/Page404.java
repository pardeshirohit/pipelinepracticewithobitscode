package testScenarios;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import helper.Constants;
import helper.LoggerHelper;
import pageObject.CommonModel;
import pageObject.Page404Model;

public class Page404 extends TestBase {

	
	Logger log = LoggerHelper.getLogger(Page404.class);
	
	//Checking Page Not Found Message is showing properly
	@Test(priority=1)
	public void checkPageNotFoundMessage()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			Page404Model page404Model=new Page404Model(driver);
			driver.get(Constants.Page404Link);
			if(page404Model.checkingVisibilityOfPageNotFoundMsg()==false)
			{
				log.error("\n\t Page Not Found Message is not showing on the 404 page");
				softAssert.fail("\n\t Page Not Found Message is not showing on the 404 page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	
	@Test(priority=2)
	public void commonElementsArePresentOn404Page()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.checkingVisibilityOfSiteLogoInSideBar()==false)
			{
				log.error("\n\t Site Logo In Sidebar is not showing On 404 Page");
				softAssert.fail("\n\t Site Logo In Sidebar is not showing On 404 Page");
			}
			if(commonModel.checkingVisibilityOfCategory()==false)
			{
				log.error("\n\t category links are not showing On 404 Page");
				softAssert.fail("\n\t category links are not showing On 404 Page");
			}
			if(commonModel.checkingVisibilityOfSearchBoxPresentInTheSidebar()==false)
			{
				log.error("\n\t Search Box is not showing On 404 Page");
				softAssert.fail("\n\t Search Box is not showing On 404 Page");
			}
			if(commonModel.checkingVisibilityOfCalendarPresentIntheSidebar()==false)
			{
				log.error("\n\t calendar is not showing On 404 Page");
				softAssert.fail("\n\t calendar is not showing On 404 Page");
			}
			if(commonModel.checkingVisibilityOfSignUpSectionPresentInTheSidebar()==false)
			{
				log.error("\n\t Sign Up section is not showing On 404 Page");
				softAssert.fail("\n\t Sign Up section is not showing On 404 Page");
			}
			if(commonModel.checkingVisibilityOffeaturedObituariesSectionPresentInTheSidebar()==false)
			{
				log.error("\n\t Featured Obituaries section is not showing On 404 Page");
				softAssert.fail("\n\t Featured Obituaries section is not showing On 404 Page");
			}
			if(commonModel.checkingVisibilityOfHeader()==false)
			{
				log.error("\n\t Header is not showing On 404 Page");
				softAssert.fail("\n\t Header is not showing On 404 Page");
			}
			if(commonModel.checkingVisibilityOfFooter()==false)
			{
				log.error("\n\t Footer is not showing On 404 Page");
				softAssert.fail("\n\t Footer is not showing On 404 Page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	
	//Checking Click Here Link Is Working Properly
	@Test(priority=3)
	public void checkClickHereLinkWorksProperly()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			Page404Model page404Model=new Page404Model(driver);
			page404Model.clickOnClickHereLink();
			if(driver.getCurrentUrl().contains(Constants.URL)==false)
			{
				log.error("\n\t click Here link is not working on the 404 page");
				softAssert.fail("\n\t click Here link is not working on the 404 page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
}
