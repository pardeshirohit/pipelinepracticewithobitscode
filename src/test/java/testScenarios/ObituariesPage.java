package testScenarios;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import helper.LoggerHelper;
import pageObject.CommonModel;
import pageObject.HomePageModel;
import pageObject.ObituariesPageModel;

public class ObituariesPage extends TestBase {

	Logger log = LoggerHelper.getLogger(ObituariesPage.class);
	
	//Check elements present on the Obituatries page are showing properly.
	@Test(priority=1)
	public void checkElementsPresentOnTheObituariesPage()
	{
		SoftAssert softAssert = new SoftAssert();
		try 
		{
			ObituariesPageModel obituariesPageModel=new ObituariesPageModel(driver);
			HomePageModel homePageModel=new HomePageModel(driver);
			
			//Check Categories Present on the obiruaries Page are showing properly
			homePageModel.obituariesPostLink.click();
			
			if(obituariesPageModel.familyPlacedCategoryHomepage.isDisplayed()==false )
			{
				log.error("\n\t familyPlacedCategory Category link is not showing properly on Obituaries page");
				softAssert.fail("\n\t familyPlacedCategory Category link is not showing properly on Obituaries page");
			}
			
			if(obituariesPageModel.deathNoticesCategoryHomepage.isDisplayed()==false )
			{
				log.error("\n\t Death Notices Category link is not showing properly on Obituaries page");
				softAssert.fail("\n\t Death Notices Category link is not showing properly on Obituaries page");
			}
			
			//Check social media icons are showing properly on obituaries page.
			if(obituariesPageModel.shareOnFacebookBtn.isDisplayed()==false)
			{
				log.error("\n\t Share on Facebook Button is not showing on Obituaries Page");
				softAssert.fail("\n\t Share on Facebook Button is not showing on Obituaries Page");
			}
			
			if(obituariesPageModel.shareViaEmailBtn.isDisplayed()==false)
			{
				log.error("\n\t Share Via Email Button is not showing on Obituaries Page");
				softAssert.fail("\n\t Share Via Email Button is not showing on Obituaries Page");
			}
			
			//Check Condolence message showing on Obituaries Page
			if(obituariesPageModel.condolenceMessageShowingOnObituariesPage.isDisplayed()==false)
			{
				log.error("\n\t Condolence Message is not showing properly on Obituaries Page");
				softAssert.fail("\n\t Condolence Message is not showing properly on Obituaries Page");
			}
		} 
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
		
	}
	@Test(priority=2)
	public void checkCommonContentPresentOnTheObituariesPage()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			//checking for search functionality is showing properly
			if(commonModel.searchBoxPresentInTheObituaryPageSidebar.isDisplayed()==false)
			{
				log.error("\n\t Search Box is not showing properly on Obituaries page");
				softAssert.fail("\n\t Search Box is not showing properly on Obituaries page");
			}
		
			//checking for Calendar functionality is showing properly
			if(commonModel.calendarPresentIntheSidebar.isDisplayed()==false)
			{
				log.error("\n\t Calendar Widget is not showing properly on Obituaries page");
				softAssert.fail("\n\t Calendar Widget is not showing properly on Obituaries page");
			}
		
			//checking for Featured Obituaries section is showing properly
			if(commonModel.featuredObituariesSectionPresentInTheSidebar.isDisplayed()==false)
			{
				log.error("\n\t Featured Obituaries Section is not showing properly on Obituaries page");
				softAssert.fail("\n\t Featured Obituaries Section is not showing properly on Obituaries page");
			}
			
			//checking for header
			if(commonModel.header.isDisplayed()==false)
			{
				log.error("\n\t Header is not showing properly on Obituaries page");
				softAssert.fail("\n\t Header is not showing properly on Obituaries page");
			}
			
			//checking for footer
			if(commonModel.footer.isDisplayed()==false)
			{
				log.error("\n\t Footer is not showing properly on Obituaries page");
				softAssert.fail("\n\t Footer is not showing properly on Obituaries page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
}