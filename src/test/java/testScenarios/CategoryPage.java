package testScenarios;

import org.apache.log4j.Logger;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import helper.Constants;
import helper.LoggerHelper;
import pageObject.CategoryPageModel;
import pageObject.CommonModel;

public class CategoryPage extends TestBase {
	CategoryPageModel categoryPageModel=new CategoryPageModel(driver);
	Logger log = LoggerHelper.getLogger(CategoryPage.class);

	//Checking for Family Placed Obituaries Category
	@Test(priority=1)
	public void checkTotalPostPresentOnTheFamilyPlacedCategoryPage()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			commonModel.familyPlacedObituariesCategoryLinkPresentInTheHomepageSidebar.click();
			if(categoryPageModel.checkTotalPostPresentOnTheFamilyPlacedCategoryPage()==0)
			{
				log.error("\n\t 20 posts are not showing on the family placed categories page");
				softAssert.fail("\n\t 20 posts are not showing on the family placed categories page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=2)
	public void elementsArePresentOnFamilyPlacedCategoryPage()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.checkingVisibilityOfSiteLogoInSideBar()==false)
			{
				log.error("\n\t Site Logo In Sidebar is not showing On Family Placed Category Page");
				softAssert.fail("\n\t Site Logo In Sidebar is not showing On Family Placed Category Page");
			}
			if(commonModel.checkingVisibilityOfCategory()==false)
			{
				log.error("\n\t category links are not showing On Family Placed Category Page");
				softAssert.fail("\n\t category links are not showing On Family Placed Category Page");
			}
			if(commonModel.checkingVisibilityOfSearchBoxPresentInTheSidebar()==false)
			{
				log.error("\n\t Search Box is not showing On Family Placed Category Page");
				softAssert.fail("\n\t Search Box is not showing On Family Placed Category Page");
			}
			if(commonModel.checkingVisibilityOfCalendarPresentIntheSidebar()==false)
			{
				log.error("\n\t calendar is not showing On Family Placed Category Page");
				softAssert.fail("\n\t calendar is not showing On Family Placed Category Page");
			}
			if(commonModel.checkingVisibilityOfSignUpSectionPresentInTheSidebar()==false)
			{
				log.error("\n\t Sign Up section is not showing On Family Placed Category Page");
				softAssert.fail("\n\t Sign Up section is not showing On Family Placed Category Page");
			}
			if(commonModel.checkingVisibilityOffeaturedObituariesSectionPresentInTheSidebar()==false)
			{
				log.error("\n\t Featured Obituaries section is not showing On Family Placed Category Page");
				softAssert.fail("\n\t Featured Obituaries section is not showing On Family Placed Category Page");
			}
			if(commonModel.checkingVisibilityOfHeader()==false)
			{
				log.error("\n\t Header is not showing On Family Placed Category Page");
				softAssert.fail("\n\t Header is not showing On Family Placed Category Page");
			}
			if(commonModel.checkingVisibilityOfFooter()==false)
			{
				log.error("\n\t Footer is not showing On Family Placed Category Page");
				softAssert.fail("\n\t Footer is not showing On Family Placed Category Page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=3)
	public void checkReadMoreLinkPresentOnTheFamilyPlacedCategoryPageIsWorkingProperly()
	{
		CategoryPageModel categoryPageModel=new CategoryPageModel(driver);
		categoryPageModel.checkReadMoreLinkWorkingProperlyOnCategoryPage();
	}
	@Test(priority=4)
	public void checkOlderAndNewerBtnsPresentOnFamilyPlacedCategoryPage()
	{
		CategoryPageModel categoryPageModel=new CategoryPageModel(driver);
		categoryPageModel.checkOlderAndNewerPostButtonsOnCategoryPage();
		driver.navigate().to(Constants.URL);
	}
	
	//Checking for Death Notices Category
	@Test(priority=5)
	public void checkTotalPostPresentOnTheDeathNoticesCategoryPage()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			commonModel.deathNoticesCategoryLinkPresentInTheSidebar.click();
			if(categoryPageModel.checkTotalPostPresentOnTheDeathNoticesCategoryPage()==0)
			{
				log.error("\n\t 20 posts are not showing on Death Notices Category page");
				softAssert.fail("\n\t 20 Posts are not showing on the Death Notices Category Page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=6)
	public void elementsArePresentOnDeathNoticesCategoryPage()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.checkingVisibilityOfSiteLogoInSideBar()==false)
			{
				log.error("\n\t Site Logo In Sidebar is not showing On Death Notices Category Page");
				softAssert.fail("\n\t Site Logo In Sidebar is not showing On Death Notices  Page");
			}
			if(commonModel.checkingVisibilityOfCategory()==false)
			{
				log.error("\n\t category links are not showing On Death Notices Category Page");
				softAssert.fail("\n\t category links are not showing On Death Notices Category Page");
			}
			if(commonModel.checkingVisibilityOfSearchBoxPresentInTheSidebar()==false)
			{
				log.error("\n\t Search Box is not showing On Death Notices Category Page");
				softAssert.fail("\n\t Search Box is not showing On Death Notices Category Page");
			}
			if(commonModel.checkingVisibilityOfCalendarPresentIntheSidebar()==false)
			{
				log.error("\n\t calendar is not showing On Death Notices Category Page");
				softAssert.fail("\n\t calendar is not showing On Death Notices Category Page");
			}
			if(commonModel.checkingVisibilityOfSignUpSectionPresentInTheSidebar()==false)
			{
				log.error("\n\t Sign Up section is not showing On Death Notices Category Page");
				softAssert.fail("\n\t Sign Up section is not showing On Death Notices Category Page");
			}
			if(commonModel.checkingVisibilityOffeaturedObituariesSectionPresentInTheSidebar()==false)
			{
				log.error("\n\t Featured Obituaries section is not showing On Death Notices Category Page");
				softAssert.fail("\n\t Featured Obituaries section is not showing On Death Notices Category Page");
			}
			if(commonModel.checkingVisibilityOfHeader()==false)
			{
				log.error("\n\t Header is not showing On Death Notices Category Page");
				softAssert.fail("\n\t Header is not showing On Death Notices Category Page");
			}
			if(commonModel.checkingVisibilityOfFooter()==false)
			{
				log.error("\n\t Footer is not showing On Death Notices Category Page");
				softAssert.fail("\n\t Footer is not showing On Death Notices Category Page");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=7)
	public void checkReadMoreLinkPresentOnTheDeathNoticesCategoryPageIsWorkingProperly()
	{
		CategoryPageModel categoryPageModel=new CategoryPageModel(driver);
		categoryPageModel.checkReadMoreLinkWorkingProperlyOnCategoryPage();
	}
	@Test(priority=8)
	public void checkOlderAndNewerBtnsPresentOnDeathNoticesCategoryPage()
	{
		CategoryPageModel categoryPageModel=new CategoryPageModel(driver);
		categoryPageModel.checkOlderAndNewerPostButtonsOnCategoryPage();
	}
}
