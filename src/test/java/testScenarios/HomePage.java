package testScenarios;

import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import helper.Constants;
import helper.Helper;
import helper.LoggerHelper;
import pageObject.CommonModel;
import pageObject.HomePageModel;
import pageObject.ObituariesPageModel;

public class HomePage extends TestBase {
	Logger log = LoggerHelper.getLogger(HomePage.class);
	//Check Todays Obituaries section is showing properly
	@Test(priority=1)
	public void checkTodaysObituariesSection()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			HomePageModel homePageModel = new HomePageModel(driver);
	
			// Check for Todays Obituaries Title
		
			if(homePageModel.todaysObituariesTitle.isDisplayed()==false)
			{
				log.error("\n\t Todays Obituaries title is not showing");
				softAssert.fail("\n\t Todays Obituaries Title is not showing");
			}
		
			// Check for Todays Obituaries Posts
		
			if(homePageModel.obituariesPosts.isEmpty()==true)
			{
				log.error("\n\t Posts are not showing in the Todays Obituaries section");
				softAssert.fail("\n\t Posts are not showing in the Todays Obituaries section");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}

	// Check in Todays Obituaries section only current days obituaries are showing
	@Test(priority=2)
	// Check For Todays Date
	public void checkOnlyTodaysObituariesAreShowing()
	{
		// Check For Todays Date
		TimeZone timeZone=TimeZone.getTimeZone("US/Hawaii");
		Helper helper=new Helper();
		helper.getTodayDate("MMMM dd,yyyy", timeZone);
		
		//Take any post randomly and check post date and todays date is matching
		HomePageModel homePageModel=new HomePageModel(driver);
		homePageModel.getDatePresentInThePost();
		
	}
	/*Update softAssert From Here*/
	// Check Need To Place An Obituaries Section Is Showing Properly
	@Test(priority=3)
	public void checkneedToPlaceAnObituariesSectionIsShowing()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			HomePageModel homePageModel=new HomePageModel(driver);
			if(homePageModel.needToPlaceAnObituaries.isDisplayed()==false)
			{
				log.error("\n\t Need to Place An Obituaries Section is not showing Showing Properly on Homepage");
				softAssert.fail("\n\t Need to Place An Obituaries Section is not showing Showing Properly on Homepage");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
		
	}
	// Check footer is Showing Properly 
	@Test(priority=4)
	public void checkFooterIsShowingProperlyOnHomepage()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.footer.isDisplayed()==false)
			{
				log.error("\n\t Footer is not showing properly on the Homepage");
				softAssert.fail("\n\t Footer is not showing properly on the Homepage\"");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	// Check After clicking on the obituaries a user redirects to the obituaries page
	@Test(priority=5)
	public void checkObituariesLink()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			//** Click on the obituaries title
			HomePageModel homePageModel=new HomePageModel(driver);
			homePageModel.clickOnObituariesPostLink();
			ObituariesPageModel obituariesPageModel=new ObituariesPageModel(driver);
			if(obituariesPageModel.familyPlacedCategoryHomepage.isDisplayed()==false)
			{
				log.error("\n\t Clicking on Obituaries title a user is not redirects to the Obituaries page");
				softAssert.fail("\n\t Clicking on Obituaries title a user is not redirects to the Obituaries page");
			}
			driver.navigate().back();
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	// Check After clicking on the Obituaries Category link a user redirects to the obituaries page
	@Test(priority=6)
	public void checkObiruariesCategorylink()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			HomePageModel homePageModel=new HomePageModel(driver);
			homePageModel.categoryLinkPresentinthePost.click();
			if(driver.getCurrentUrl().contains(Constants.categoryPageURL)==false)
			{
				log.error("\n\t Clicking on Obituaries category a user is not redirects to the Categories page");
				softAssert.fail("\n\t Clicking on Obituaries category a user is not redirects to the Categories page");
			}
			driver.navigate().back();
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	//Check SiteLogo Present In the Sidebar
	@Test(priority=7)
	public void checkSiteLogoPresentOnTheSidebar()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.siteLogoInSidebar.isDisplayed()==false)
			{
				log.error("\n\t Site logo is not displayed in the Homepage sidebar");
				softAssert.fail("\n\t Site logo is not displayed in the Homepage sidebar");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=8)
	public void checkCategoryLinksAreShowingOnTheSidebar()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.familyPlacedObituariesCategoryLinkPresentInTheHomepageSidebar.isDisplayed()==false)
			{
				log.error("\n\t Family Placed Category Link is not showing in the Homepage Sidebar");
				softAssert.fail("\n\t Family Placed Category Link is not showing in the Homepage Sidebar");
			}
			if(commonModel.deathNoticesCategoryLinkPresentInTheSidebar.isDisplayed()==false)
			{
				log.error("\n\t Death Notices Category Link is not showing in the Homepage Sidebar");
				softAssert.fail("\n\t Death Notices Category Link is not showing in the Homepage Sidebar");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=9)
	public void checkSearchSectionIsShowingOnTheSidebar()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.searchBoxPresentInTheSidebar.isDisplayed()==false)
			{
				log.error("\n\t Search Bar is not showing in the sidebar");
				softAssert.fail("\n\t Search Bar is not showing in the sidebar");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=10)
	public void checkCalendarIsShowingOnTheSidebar()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.calendarPresentIntheSidebar.isDisplayed()==false)
			{
				log.error("\n\t Calendar is not showing in the Homepage Sidebar");
				softAssert.fail("\n\t Calendar is not showing in the Homepage Sidebar");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=11)
	public void checkSignUpSectionIsShowingOnTheSidebar()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.signUpSectionPresentInTheSidebar.isDisplayed()==false)
			{
				log.error("\n\t Sign Up section is not showing in the sidebar");
				softAssert.fail("\n\t Sign Up section is not showing in the sidebar");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=12)
	public void checkFeaturedObituariesSectionIsShowingProperly()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.featuredObituariesSectionPresentInTheSidebar.isDisplayed()==false)
			{
				log.error("\n\t Featured Obituaries Section is not Showing in the Homepage Sidebar");
				softAssert.fail("\n\t Featured Obituaries Section is not showing in the Homepage Sidebar");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
	@Test(priority=13)
	public void checkHeaderisShowing()
	{
		SoftAssert softAssert=new SoftAssert();
		try
		{
			CommonModel commonModel=new CommonModel(driver);
			if(commonModel.header.isDisplayed()==false)
			{
				log.error("\n\t Header is not showing on the HomePage");
				softAssert.fail("\n\t Header is not showing on the HomePage");
			}
		}
		catch (Exception e) {
			log.error(e.toString());
			softAssert.fail(e.toString());
		}
		finally {
			softAssert.assertAll();
		}
	}
}
