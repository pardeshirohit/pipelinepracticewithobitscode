package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class ObituariesPageModel extends PageObjectBase{

	public ObituariesPageModel(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, ObituariesPageModel.this);
	}
	
		@FindBy(how=How.XPATH, using="(//span[@class='single-category-item'])[1]")
		public WebElement familyPlacedCategoryHomepage;
		
		@FindBy(how=How.XPATH, using="(//span[@class='single-category-item'])[2]")
		public WebElement deathNoticesCategoryHomepage;
		
		@FindBy(how=How.ID, using="fbshareBtn")
		public WebElement shareOnFacebookBtn;
		
		@FindBy(how=How.XPATH, using="//button[@class='share-via-email w-100']")
		public WebElement shareViaEmailBtn;
		
		@FindBy(how=How.XPATH, using="//p[@class='p-2']")
		public WebElement condolenceMessageShowingOnObituariesPage;
}
