package pageObject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helper.LoggerHelper;
import testScenarios.HomePage;

public class HomePageModel extends PageObjectBase {
	
	Logger log = LoggerHelper.getLogger(HomePageModel.class);
	
	//PageObjectBase pageObjectBase;

	public HomePageModel(WebDriver driver) 
	{
		super(driver);
		PageFactory.initElements(driver, HomePageModel.this);
	}
	//Webelement for todays obituaries post title
	@FindBy(how=How.XPATH, using="//header[@class=\"titlebox\"]")
	public WebElement todaysObituariesTitle;
	
	//Creating list for obituaries post present on the homepage
	@FindBy(how=How.TAG_NAME, using="article")
	public List<WebElement> obituariesPosts;
	
	//Webelement for date present in the post
	@FindBy(how=How.XPATH, using="(//section[@class='homepage-post-credit '])[1]")
	public WebElement datePresentInThePost;

	//WebElement for obituaries post link
	@FindBy(how=How.XPATH, using="(//a[@class='homepage-post-title'])[1]")
	public WebElement obituariesPostLink;
	
	//Getting Todays date present in the post
	public void getDatePresentInThePost()
	{
		String dateInThePost= datePresentInThePost.getText();
		int indexOfFirstText=dateInThePost.indexOf("On");
		log.info("\n \t Date showing in the obits post="+dateInThePost.substring(indexOfFirstText+3));
	}

	//WebElement for NeedToPlaceAnObituary section present on the Homepage
	@FindBy(how=How.XPATH, using="//div[@class='container-fluid clear px-3 px-sm-5 px-lg-3']")
	public WebElement needToPlaceAnObituaries;

	//WebElement for Category Link Present in the Post
	@FindBy(how=How.XPATH, using="(//section[@class='homepage-post-metadata mt-1'])[1]")
	public WebElement categoryLinkPresentinthePost;
	
	public void clickOnObituariesPostLink()
	{
		waitAndClick(obituariesPostLink, 20);
	}
}