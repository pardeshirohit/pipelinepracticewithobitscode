package pageObject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helper.Constants;
import helper.Helper;
import helper.LoggerHelper;

public class CommonModel extends PageObjectBase {
	Logger log = LoggerHelper.getLogger(CommonModel.class);

	public CommonModel(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, CommonModel.this);
	}
	
	// WebElement for Footer
	@FindBy(how=How.XPATH, using="//div[@class='footer-content-area container-fluid clear px-0']")
	public WebElement footer;
	
	// Elements Present in the Sidebar
	
	//1. WebElement for Site Logo Present in the sidebar
	@FindBy(how=How.XPATH, using="(//a[@class='logo-header'])[2]")
	public WebElement siteLogoInSidebar;
	
	//2. Navigation menu present in the sidebar
	// 2.1 WebElement for familyPlacedObituariesCategoryLink
	@FindBy(how=How.XPATH, using="(//li[@class='menu-item menu-item-type-taxonomy menu-item-object-category menu-item-124474'])[2]")
	public WebElement familyPlacedObituariesCategoryLinkPresentInTheHomepageSidebar;
	
	//2.2 WebElement for Death Notices Category link
	@FindBy(how=How.XPATH, using="(//li[@class='menu-item menu-item-type-taxonomy menu-item-object-category menu-item-124475']//a)[2]")
	public WebElement deathNoticesCategoryLinkPresentInTheSidebar;
	
	//3. WebElement Search Box present in the sidebar
	@FindBy(how=How.ID, using="search-6")
	public WebElement searchBoxPresentInTheSidebar;

	//4. WebElement for SearchBox Present In The Obituary Page Sidebar
	@FindBy(how=How.XPATH, using="(//input[@name='s'])[2]")
	public WebElement searchBoxPresentInTheObituaryPageSidebar;

	//5. WebElement for Calendar present in the sidebar
	@FindBy(how=How.XPATH, using="(//li[@class='widget widget_calendar'])[2]")
	public WebElement calendarPresentIntheSidebar;
	
	//6. WebElement for Sign-up section present in the sidebar
	@FindBy(how=How.XPATH, using="//div[@class='sign-up-area text-center']")
	public WebElement signUpSectionPresentInTheSidebar;

	//6. WebElement for Featured Obituaries section present in the sidebar
	@FindBy(how=How.XPATH, using="//div[@class='col-12']")
	public WebElement featuredObituariesSectionPresentInTheSidebar;
	
	//WebElement for Header present in the sidebar
	@FindBy(how=How.XPATH, using="//ul[@class='primary-menu col-12 mb-0']")
	public WebElement header;
	
	//WebElement For Input Box present in the search functionality
	@FindBy(how=How.XPATH, using="(//input[@name='s'])[2]")
	public WebElement inputBoxPresentInTheSearch;
	
	//WebElement for Button present in the search functionality
	@FindBy(how=How.XPATH, using="(//button[@type='button'])[4]")
	public WebElement goButtonPresentInTheSearch;
	
	//WebElement for posts present in the search result
	@FindBy(how=How.XPATH, using="(//a[@class='homepage-post-title'])[1]")
	public WebElement postInSearchResult;
	
	//WebElement for SignUp Email Input Box
	@FindBy(how=How.ID, using="joinEmail")
	public WebElement signUpEmailInputBox;
	
	//WebElement for SignMeUp Button
	@FindBy(how=How.ID, using="SubmitButton")
	public WebElement signMeUpBtn;
	
	//WebElement for SignUp Error
	@FindBy(how=How.ID, using="joinError")
	public WebElement signUpError;
	
	//WebElement for Date Archive Black Bar
	@FindBy(how=How.XPATH, using="//header[@class='archive-title row w-100 m-auto text-center']")
	public WebElement dateArchiveBlackBar;
	
	//WebElement for Category Section
	@FindBy(how=How.XPATH, using="//ul[@id='menu-obituaries-category-1']")
	public WebElement categorySection;
	
	// Checking visiblity of elements
	//1.checking visibility of site logo present in the sidebar
	public boolean checkingVisibilityOfSiteLogoInSideBar()
	{
		return siteLogoInSidebar.isDisplayed();
	}
	
	//2.checking visibility of category Section
	public boolean checkingVisibilityOfCategory()
	{
		return categorySection.isDisplayed();
	}
	
	//3. checking visibility of search Box Present In The Sidebar
	public boolean checkingVisibilityOfSearchBoxPresentInTheSidebar()
	{
		return searchBoxPresentInTheSidebar.isDisplayed();
	}
	
	//4. checking visibility of calendar Present In the Sidebar
	public boolean checkingVisibilityOfCalendarPresentIntheSidebar()
	{
		return calendarPresentIntheSidebar.isDisplayed();
	}
	
	//5. checking visibility of signUp Section Present In The Sidebar
	public boolean checkingVisibilityOfSignUpSectionPresentInTheSidebar()
	{
		return calendarPresentIntheSidebar.isDisplayed();
	}
	
	//6. checking visibility of featured Obituaries Section Present In The Sidebar
	public boolean checkingVisibilityOffeaturedObituariesSectionPresentInTheSidebar()
	{
		return featuredObituariesSectionPresentInTheSidebar.isDisplayed();
	}
	
	//7. Checking Visibility of Header
	public boolean checkingVisibilityOfHeader()
	{
		return header.isDisplayed();
	}
	
	//8. Checking Visibility of Footer
	public boolean checkingVisibilityOfFooter()
	{
		return footer.isDisplayed();
	}
	// Verifying Search Functionality
	public void checkSearchFunctionalityIsWorkingProperly()
	{
		inputBoxPresentInTheSearch.sendKeys("martin");
		goButtonPresentInTheSearch.click();
		
		String keywordPresentInSearchResult=postInSearchResult.getText();
		
		if(keywordPresentInSearchResult.toLowerCase().contains("martin"))
		{
			log.info("\n\t Search Functionality is working properly");
		}
		else
		{
			log.info("\n\t Search Functionality is not working properly");
		}
		driver.navigate().back();
	}
	
	// Verifying SignUp Functionality
	public void checkSignUpFunctionalityIsWorkingProperly()
	{
		signUpEmailInputBox.click();
		signUpEmailInputBox.sendKeys("TestingEmail");
		signMeUpBtn.click();
		
		if(signUpError.isDisplayed())
		{
			log.info("\n\t Sign Up functionality is working properly");
		}
		else
		{
			log.info("\n\t Sign Up functionality is not working properly");
		}
	}
	// Verifying Calendar Functionality
	public void checkCalendarFunctionalityIsWorkingProperly() throws InterruptedException
	{
			Helper helper=new Helper();
			List <WebElement>dateColumn=calendarPresentIntheSidebar.findElements(By.tagName("td"));
		
			for(WebElement presentDate:dateColumn)
			{
				if(presentDate.getText().equals(helper.getCurrentDay()))
				{
					Actions actions=new Actions(driver);
					actions.moveToElement(presentDate).click().build().perform();
					break;
				}
			}
			
			if(dateArchiveBlackBar.isDisplayed())
			{
				log.info("\n\t Calendar is working properly");
			}
			else
			{
				log.info("\n\t Calendar is not working properly");
			}
			driver.navigate().back();
	}
}
