package pageObject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helper.Constants;
import helper.LoggerHelper;
import testScenarios.CategoryPage;

public class CategoryPageModel extends PageObjectBase {
	Logger log = LoggerHelper.getLogger(CategoryPage.class);
	CommonModel commonModel=new CommonModel(driver);

	public CategoryPageModel(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, CategoryPageModel.this);
	}

	//Creating List For Total Post Present on the Category Page
	public List <WebElement> totalPostsOnCategorypage()
	{
		return waitForElementsToBeVisible(driver.findElements(By.xpath("//section[@class='homepage-post-metadata mt-1']")), 20);
	}
	
	//WebElement For Read More Link present on the Category Page
	@FindBy(how=How.XPATH, using="(//a[@class='homepage-post-readmore'])[1]")
	public WebElement readMoreLink;
	
	//WebElement For Older Post Button
	@FindBy(how=How.CLASS_NAME, using="older")
	public WebElement olderPostButton;
	
	//WebElement For Newer Post Button
	@FindBy(how=How.CLASS_NAME, using="newer")
	public WebElement newerPostButton;
	
	// Checking 20 Posts are showing on category page or not
	public int checkTotalPostPresentOnTheFamilyPlacedCategoryPage()
	{
		int is20PostAreShowingOnFamilyPlacedCategoryPage=0; //0=false 1=true
		if(driver.getCurrentUrl().contains(Constants.familyPlacedCategoryUrl))
		{
			if(totalPostsOnCategorypage().size()!=20)
			{
				is20PostAreShowingOnFamilyPlacedCategoryPage=0;
			}  
			else
			{
				is20PostAreShowingOnFamilyPlacedCategoryPage=1;
			}
		}
		return is20PostAreShowingOnFamilyPlacedCategoryPage;
	}
		public int checkTotalPostPresentOnTheDeathNoticesCategoryPage()
		{
			int total20PostAreShowingOnDeathNoticesCategoryPage=0; //0=false 1=true
			
			if(driver.getCurrentUrl().contains(Constants.deathNoticesCategoryUrl))
			{
				if(totalPostsOnCategorypage().size()!=20)
				{
					return total20PostAreShowingOnDeathNoticesCategoryPage=0;
				}  
				else
				{
					return total20PostAreShowingOnDeathNoticesCategoryPage=1;
				}
		}
			return total20PostAreShowingOnDeathNoticesCategoryPage;
	}
	// Checking clicking on Read More link a user redirects to the obituaries page
	
	public void checkReadMoreLinkWorkingProperlyOnCategoryPage()
	{
		ObituariesPageModel obituariesPageModel=new ObituariesPageModel(driver);
		readMoreLink.click();
		if(driver.getCurrentUrl().contains(Constants.familyPlacedCategoryUrl))
		{
			if(obituariesPageModel.familyPlacedCategoryHomepage.isDisplayed())
			{
				log.info("\n\t Read More link present on the Family Placed category page is working properly");
			}
			else
			{
				log.info("\n\t Read More link present on the Family Placed category page is not working properly");
			}
		}
		if(driver.getCurrentUrl().contains(Constants.deathNoticesCategoryUrl))
		{
			if(obituariesPageModel.deathNoticesCategoryHomepage.isDisplayed())
			{
				log.info("\n\t Read More link present on the Death Notices category page is working properly");
			}
			else
			{
				log.info("\n\t Read More link present on the Death Notices category page is not working properly");
			}
		}
		driver.navigate().back();
	}
	
	// Checking Older Post and Newer Post button is working properly
	public void checkOlderAndNewerPostButtonsOnCategoryPage()
	{
		Constants constants=new Constants();
		olderPostButton.click();
		if(driver.getCurrentUrl().contains(Constants.familyPlacedCategoryUrl))
		{
			if(driver.getCurrentUrl().equals(constants.categorySecondPageLink))
			{
				log.info("\n\t Older Post Button Present on the Family Placed Category is Working properly");
			}
			else
			{
				log.info("\n\t Older Post Button Present on the Family Placed Category is Not Working properly");
			}
		}
		if(driver.getCurrentUrl().contains(Constants.deathNoticesCategoryUrl))
		{
			if(driver.getCurrentUrl().equals(constants.deathNoticesCategorySecondPageLink))
			{
				log.info("\n\t Older Post Button Present on the Death Notices Category is Working properly");
			}
			else
			{
				log.info("\n\t Older Post Button Present on the Death Notices Category is Not Working properly");
			}
		}
		newerPostButton.click();
		if(driver.getCurrentUrl().contains(Constants.familyPlacedCategoryUrl))
		{
			if(driver.getCurrentUrl().equals(constants.categoryFirstPageLink))
			{
				log.info("\n\t Newer Post Button Present on the Family Placed Category is Working properly");
			}
			else
			{
				log.info("\n\t Newer Post Button Present on the Family Placed Category is Not Working properly");
			}
		}
		if(driver.getCurrentUrl().contains(Constants.deathNoticesCategoryUrl))
		{
			if(driver.getCurrentUrl().equals(constants.deathNoticesCategoryFirstPageLink))
			{
				log.info("\n\t Newer Post Button Present on the Death Notices Category is Working properly");
			}
			else
			{
				log.info("\n\t Newer Post Button Present on the Death Notices Category is Not Working properly");
			}
		}
	}
}