package pageObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class PageObjectBase {
	public static WebDriver driver;
	private static final int LOAD_TIMEOUT = 30;
	private static final int REFRESH_RATE = 1;
	
	public PageObjectBase(WebDriver driver) 
	{
		PageObjectBase.driver = driver;
	}
	
	protected void waitForPageToLoad(ExpectedCondition pageLoadCondition) {
		Wait wait = new FluentWait(driver)
				.withTimeout(LOAD_TIMEOUT, TimeUnit.SECONDS)
				.pollingEvery(REFRESH_RATE, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		;

		wait.until(pageLoadCondition);
	}
	

	//protected abstract ExpectedCondition getPageLoadCondition();
	/**
	 * This method will wait for Element To Be Click able
	 * @param element
	 * @param timeOutInSeconds
	 * @return WebElement
	 */
	public WebElement waitForElementToBeVisible(WebElement element, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(driver,timeOutInSeconds);
		try {
			element = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOf(element));
		} catch (TimeoutException e) {
		

		}

		return element;
	}
	
	public List<WebElement> waitForElementsToBeVisible(List<WebElement> elements, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(driver,timeOutInSeconds);
		try {
			elements = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfAllElements(elements));
		} catch (TimeoutException e) {

		}

		return elements;
	}
	
	public void scrollingToPageTop()
	{
		try 
		{
			WebElement element = driver.findElement(By.xpath("//div[@class='k-pagination']"));
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.perform();
			Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	/***
	 * This method is used to Click on Element
	 * @param driver
	 * @param element
	 */
	public void waitAndClick(WebElement element, long timeOutInSeconds) {
		try {
			waitForElementToBeVisible(element, timeOutInSeconds);
			waitForElementToBeClickable(element, timeOutInSeconds);
			//Actions action = new Actions(driver);
			//action.moveToElement(element).build().perform();
			element.click();
		} catch (Exception e) {
		}

	}
	public static String isLinkBroken(URL url) throws Exception {
		String response = "";
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
			connection.connect();
			response = connection.getResponseMessage();
			connection.disconnect();
			System.out.println("Repose "+response);
			return response;
		} catch (Exception exp) {
			System.out.println(exp);
			return exp.getMessage();
		}
	}

	public static void jsClickPerform(WebDriver driver, WebElement element) {
		try {

			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public  String jsGetText(WebDriver driver,WebElement element) {
		try {
			String script = "return document.getElementById('savebtn').textContent;";
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			return executor.executeScript(script).toString();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	
	public String getSelectedLanguage1(WebDriver driver) {
		String result=null;
		try {
			 String script = "return document.getElementByXPath('//div[@class='k-flex']/label').textContent;";
			 JavascriptExecutor executor = (JavascriptExecutor)driver;
			 return executor.executeScript(script).toString();
		} catch (Exception e) {
			System.out.println(e);
		}finally {
			return result;
		}
	}	
	
	/*public void waitForLoad() {
	    new WebDriverWait(driver, 30).until((Predicate<WebDriver>) wd ->
	            ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
	}*/
	public boolean exists(By locator){
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.MILLISECONDS);
		try{
			return driver.findElement(locator).isDisplayed();
		}catch (org.openqa.selenium.NoSuchElementException
	            | org.openqa.selenium.StaleElementReferenceException
	            | org.openqa.selenium.TimeoutException e) {
			//System.out.println(e);
	        return false;
		}
	}

	public boolean exists(WebElement element){

		try{
			return element.isDisplayed();
		}catch (NoSuchElementException e){
			return false;
		}
	}


	/*public WebElement waitToAppear(int seconds,WebElement element){
		Timer timer = new Timer();
		timer.start();
		while(!timer.expired(seconds)) if(exists(element)) break;
		if(timer.expired(seconds) && !exists(element)) throw new AssertionError("Element "+element+" failed to appear within "+seconds+" seconds");
		return element;
	}*/

	/*public WebElement waitToDisappear(int seconds,WebElement element){
		Timer timer = new Timer();
		timer.start();
		while(!timer.expired(seconds)) if(!exists(element)) break;
		if(timer.expired(seconds) && exists(element)) throw new AssertionError("Element "+element+" failed to disappear within "+seconds+" seconds");
		return element;
	}
*/

	/*public static boolean isDisplayed(WebElement element) {
		try{
			return element.isDisplayed();
		}catch (NoSuchElementException e){
			return false;
		}
	}*/
	/*public void waitForpageLoad() throws SkipException, InterruptedException{

		Thread.sleep(2000);
		int seconds=Integer.parseInt(Constants.PAGE_LOAD_TIMEOUT);
		Timer timer = new Timer();
		timer.start();
		while(!timer.expired(seconds)){
			if(exists(By.id("loading"))==false) {
				return;
				}
		}
		
		throw new SkipException(driver.getCurrentUrl() + " - This page was not loaded within "+seconds+" seconds so the test case Skipped");
	}*/
	
	
	public WebElement waitForElementToBeClickable(WebElement element, long timeOutInSeconds) {
		WebDriverWait wait = new WebDriverWait(driver,timeOutInSeconds);
		try {
			element = wait.ignoring(StaleElementReferenceException.class).until(ExpectedConditions.elementToBeClickable(element));
		} catch (TimeoutException e) {
			
		}
		return element;
	}

	 public void refreshWebPage(WebDriver driver) {
			try {
				driver.navigate().refresh();
			} 
			catch (Exception e) {
			System.out.println(e);
		  }	
	  }
}
