package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class Page404Model extends PageObjectBase {

	public Page404Model(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, Page404Model.this);
	}
	
	//WebElement for page not found message
	@FindBy(how=How.XPATH, using="//div[@class='row mr-0 homepage-main']")
	public WebElement pageNotFoundMessage;
	
	//WebElement for click here link
	@FindBy(how=How.XPATH, using="//a[@href='https://hsaobitsdev.wpengine.com']")
	public WebElement clickHereLink;
	
	//Checking Visibility page not found message
	public boolean checkingVisibilityOfPageNotFoundMsg()
	{
		return pageNotFoundMessage.isDisplayed();
	}
	
	//checking click here link working properly on clickHereLink
	public void clickOnClickHereLink()
	{
		clickHereLink.click();
	}
	
}
