package helper;

public class Constants {

	public final static long explicitWait=100;
	public final static long impliciteWait = 100;
	public static String URL="https://hsaobitsdev.wpengine.com/";
	public static String categoryPageURL="https://hsaobitsdev.wpengine.com/category/";
	public static String categorySecondPageLink="https://hsaobitsdev.wpengine.com/category/family-placed-obituaries/page/2/";
	public static String categoryFirstPageLink="https://hsaobitsdev.wpengine.com/category/family-placed-obituaries/";
	public static String calendarUrl="https://hsaobitsdev.wpengine.com/2019/06/01/";
	public static String familyPlacedCategoryUrl="https://hsaobitsdev.wpengine.com/category/family-placed-obituaries/";
	public static String deathNoticesCategoryUrl="https://hsaobitsdev.wpengine.com/category/death-notices/";
	public static String deathNoticesCategoryFirstPageLink="https://hsaobitsdev.wpengine.com/category/death-notices/";
	public static String deathNoticesCategorySecondPageLink="https://hsaobitsdev.wpengine.com/category/death-notices/page/2/";
	public static String Page404Link="http://hsaobitsdev.wpengine.com/a.php";
	public static long getExplicitwait() {
		return explicitWait;
	}
	public static long getImplicitewait() {
		return impliciteWait;
	}
}
