package helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import pageObject.HomePageModel;

public class Helper {
	Logger log = LoggerHelper.getLogger(HomePageModel.class);
	
	public void getTodayDate(String dateFormat, TimeZone timeZone)
	   {
	       Date todayDate = new Date();
	       
	       //Specifying the format
	       DateFormat todayDateFormat = new SimpleDateFormat(dateFormat);
	       
	       //Setting the Timezone
	       todayDateFormat.setTimeZone(timeZone);
	       
	       //Picking the date value in the required Format
	       String strTodayDate = todayDateFormat.format(todayDate);
	       log.info("\n\t Today's Date is="+strTodayDate);
	   }
	public String getCurrentDay ()
	{
        //Create a Calendar Object
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("US/Hawaii"));
 
        //Get Current Day as a number
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
        
 
        //Integer to String Conversion
        String todayStr = Integer.toString(todayInt);
 
        return todayStr;
    }

}
