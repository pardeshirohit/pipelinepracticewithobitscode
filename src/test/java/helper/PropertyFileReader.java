package helper;
import java.io.FileInputStream;
import java.util.Properties;

public class PropertyFileReader {
	private Properties prop = null;
	public PropertyFileReader() {
		prop = new Properties();
		try {
			prop.load(new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/config/config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getWebsiteUrl() {
		return prop.getProperty("SITE_URL");
	}
	
	public int getPageLoadTimeOut() {
		return Integer.parseInt(prop.getProperty("PageLoadTimeOut"));
	}
}
