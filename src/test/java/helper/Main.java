package helper;

import java.util.TimeZone;

import testScenarios.TestBase;

public class Main {

	public static void main(String[] args) 
	{
		TimeZone timeZone=TimeZone.getTimeZone("US/Eastern");
		Helper helper=new Helper();
		helper.getTodayDate("MMMM dd,yyyy",timeZone);
	}

}
